/*
 * gui.c
 *
 *  Created on: 2 Jun. 2019
 *      Author: caoj7
 */

#include <stdint.h>
#include <stdbool.h>
#include <float.h>
#include <math.h>
#include "utils/ustdlib.h"
#include <xdc/std.h>
#include <xdc/runtime/System.h>

#include <ti/sysbios/knl/Task.h>
#include <ti/sysbios/knl/Clock.h>
#include <ti/drivers/GPIO.h>

#include "grlib/grlib.h"
#include "grlib/widget.h"
#include "grlib/canvas.h"
#include "grlib/pushbutton.h"
#include "grlib/slider.h"
//#include "grlib/line.h"

#include "driverlib/sysctl.h"

#include "drivers/frame.h"
#include "drivers/kentec320x240x16_ssd2119_spi.h"
#include "drivers/pinout.h"
#include "drivers/touch.h"

#include "gui.h"
#include "Board.h"
#include "motor.h"
#include "boost_sensors.h"
#include "ring_buffer.h"
#include "calendar.h"
#include "temperature.h"
#include "current.h"
#include <xdc/runtime/Types.h>

#define NUMBER_OF_GRAPHS 8


#define SCREEN_HEIGHT 240;
#define SCREEN_WIDTH 320
#define TOP_BANNER_HEIGHT 30
#define DEFAULT_BUTTON_HEIGHT 25
#define DEFAULT_BUTTON_WIDTH 100
#define DEFAULT_SLIDER_HEIGHT 25


#define DEFAULT_LABEL_HEIGHT 25
#define DEFAULT_LABEL_WIDTH 80
#define DEFAULT_SPACER 10


#define DEFAULT_SLIDER_X 87
#define DEFAULT_SLIDER_WIDTH 150


// array of messages that display
// 0 - TEMPERATURE AMBIENT
// 1 - TEMPERATURE MOTOR
// 2 - LIGHT
// 3 - ACCELEROMETER
// 4 - RPM
// 5 - CURRENT??????? (not implemented)

char* messages[NUMBER_OF_GRAPHS] = {"Ambient Temp (C)",
                     "Motor Temp (C)",
                     "Light (Lux)",
                     "Accel (G)",
                     "Angular velocity (RPM)",
                     "Current C (...)",
                     "Current B (...)",
                     "Current A (...)"
};

// Graphics
tContext sContext;
uint8_t motorStartStop = 1;
uint8_t motorReversed = 0;
volatile uint16_t motorSpeedDisplay;
volatile uint8_t estopDisplay;
volatile uint8_t day; // 1 = day, 0 = night
volatile uint8_t dayChangedFlag;

volatile uint8_t page; // 1 = graph, 0 = motors
volatile uint8_t selectedGraph;

volatile uint32_t clock_lastTime;

void StartStopBttnPress(tWidget *psWidget);
void ReverseForwardBttnPress(tWidget *psWidget);
void OnRPMSliderChange(tWidget* psWidget, int32_t i32Value);
void OnAccelSliderChange(tWidget* psWidget, int32_t i32Value);
void OnTempSliderChange(tWidget* psWidget, int32_t i32Value);
void OnCurrentSliderChange(tWidget* psWidget, int32_t i32Value);
void PageChangePress(tWidget *psWidget);
void GraphDisplayUpdate(tWidget *psWidget, tContext *psContext);
void GraphChangePressFwd(tWidget *psWidget);
void GraphChangePressBack(tWidget *psWidget);
void TopDisplayUpdate(void);


tCanvasWidget     g_sBackground;
tPushButtonWidget g_sStartStopBttn;
tPushButtonWidget g_sReverseForwardBttn;
tPushButtonWidget g_sRPMLabel;
tSliderWidget g_sRPMSlider;
tSliderWidget g_sAccelSlider;
tSliderWidget g_sTempSlider;
tSliderWidget g_sCurrentSlider;
tPushButtonWidget g_sRPMTitle;
tPushButtonWidget g_sAccelTitle;
tPushButtonWidget g_sTempTitle;
tPushButtonWidget g_sCurrentTitle;


tPushButtonWidget g_ChangePageBttn;

tCanvasWidget   g_sGraphBackground;
tCanvasWidget   g_sGraphField;

tPushButtonWidget g_sGraphSelectBttnFwd;
tPushButtonWidget g_sGraphSelectBttnBack;

//tLineWidget g_sVerticalAxisLine
//tLineWidget g_sHorizontalAxisLine



// The canvas widget acting as the background to the display.
Canvas(g_sBackground, WIDGET_ROOT, &g_ChangePageBttn, &g_sStartStopBttn,
       &g_sKentec320x240x16_SSD2119, 0, 31, 320, 208,
       CANVAS_STYLE_FILL, ClrLawnGreen, 0, 0, 0, 0, 0, 0);

RectangularButton(g_sStartStopBttn, &g_sBackground, &g_sReverseForwardBttn, 0,
                  &g_sKentec320x240x16_SSD2119, 40, 210, 100, 25,
                  (PB_STYLE_OUTLINE | PB_STYLE_TEXT_OPAQUE | PB_STYLE_TEXT |
                   PB_STYLE_FILL | PB_STYLE_RELEASE_NOTIFY),
                   ClrDarkBlue, ClrBlue, ClrWhite, ClrWhite,
                   g_psFontCmss16b, "Start", 0, 0, 0, 0, StartStopBttnPress);

RectangularButton(g_sReverseForwardBttn, &g_sBackground, &g_sRPMLabel, 0,
                  &g_sKentec320x240x16_SSD2119, 180, 210, 100, 25,
                  (PB_STYLE_OUTLINE | PB_STYLE_TEXT_OPAQUE | PB_STYLE_TEXT |
                   PB_STYLE_FILL | PB_STYLE_RELEASE_NOTIFY),
                   ClrDarkBlue, ClrBlue, ClrWhite, ClrWhite,
                   g_psFontCmss16b, "Reverse", 0, 0, 0, 0, ReverseForwardBttnPress);

RectangularButton(g_sRPMLabel, &g_sBackground, &g_sRPMTitle, 0,
                  &g_sKentec320x240x16_SSD2119, 240, 41, DEFAULT_LABEL_WIDTH, 30,
                  (PB_STYLE_OUTLINE | PB_STYLE_TEXT_OPAQUE | PB_STYLE_TEXT |
                   PB_STYLE_FILL | PB_STYLE_RELEASE_NOTIFY),
                   ClrDarkBlue, ClrDarkBlue, ClrWhite, ClrWhite,
                   g_psFontCmss16b, "0", 0, 0, 0, 0, 0);

RectangularButton(g_sRPMTitle, &g_sBackground, &g_sAccelTitle, 0,
                  &g_sKentec320x240x16_SSD2119, 3, 41, DEFAULT_LABEL_WIDTH, 30,
                  (PB_STYLE_OUTLINE | PB_STYLE_TEXT_OPAQUE | PB_STYLE_TEXT |
                   PB_STYLE_FILL | PB_STYLE_RELEASE_NOTIFY),
                   ClrDarkBlue, ClrDarkBlue, ClrWhite, ClrWhite,
                   g_psFontCmss16b, "RPM", 0, 0, 0, 0, 0);

RectangularButton(g_sAccelTitle, &g_sBackground, &g_sTempTitle, 0,
                  &g_sKentec320x240x16_SSD2119, 3, 76, DEFAULT_LABEL_WIDTH, 30,
                  (PB_STYLE_OUTLINE | PB_STYLE_TEXT_OPAQUE | PB_STYLE_TEXT |
                   PB_STYLE_FILL | PB_STYLE_RELEASE_NOTIFY),
                   ClrDarkBlue, ClrDarkBlue, ClrWhite, ClrWhite,
                   g_psFontCmss16b, "ACCEL", 0, 0, 0, 0, 0);

RectangularButton(g_sTempTitle, &g_sBackground, &g_sCurrentTitle, 0,
                  &g_sKentec320x240x16_SSD2119, 3, 111, DEFAULT_LABEL_WIDTH, 30,
                  (PB_STYLE_OUTLINE | PB_STYLE_TEXT_OPAQUE | PB_STYLE_TEXT |
                   PB_STYLE_FILL | PB_STYLE_RELEASE_NOTIFY),
                   ClrDarkBlue, ClrDarkBlue, ClrWhite, ClrWhite,
                   g_psFontCmss16b, "TEMP", 0, 0, 0, 0, 0);

RectangularButton(g_sCurrentTitle, &g_sBackground, &g_sRPMSlider, 0,
                  &g_sKentec320x240x16_SSD2119, 3, 146, DEFAULT_LABEL_WIDTH, 30,
                  (PB_STYLE_OUTLINE | PB_STYLE_TEXT_OPAQUE | PB_STYLE_TEXT |
                   PB_STYLE_FILL | PB_STYLE_RELEASE_NOTIFY),
                   ClrDarkBlue, ClrDarkBlue, ClrWhite, ClrWhite,
                   g_psFontCmss16b, "CURRENT", 0, 0, 0, 0, 0);

Slider(g_sRPMSlider, &g_sBackground, &g_sAccelSlider, 0,
             &g_sKentec320x240x16_SSD2119, DEFAULT_SLIDER_X, 41, DEFAULT_SLIDER_WIDTH, 30, 300, 2500, 1000,
             (SL_STYLE_FILL | SL_STYLE_BACKG_FILL | SL_STYLE_OUTLINE|
              SL_STYLE_TEXT | SL_STYLE_BACKG_TEXT),
              ClrGray, ClrBlack, ClrSilver, ClrWhite, ClrWhite,
              g_psFontCmss16b, "0", 0, 0, OnRPMSliderChange);

Slider(g_sAccelSlider, &g_sBackground, &g_sTempSlider, 0,
             &g_sKentec320x240x16_SSD2119, DEFAULT_SLIDER_X, 76,
             DEFAULT_SLIDER_WIDTH, 30, 1200, 2000, 1200,
             (SL_STYLE_FILL | SL_STYLE_BACKG_FILL | SL_STYLE_OUTLINE|
              SL_STYLE_TEXT | SL_STYLE_BACKG_TEXT),
              ClrGray, ClrBlack, ClrSilver, ClrWhite, ClrWhite,
              g_psFontCmss16b, "0", 0, 0, OnAccelSliderChange);

Slider(g_sTempSlider, &g_sBackground, &g_sCurrentSlider, 0,
           &g_sKentec320x240x16_SSD2119, DEFAULT_SLIDER_X, 111, DEFAULT_SLIDER_WIDTH, 30, 0, 50, 40,
             (SL_STYLE_FILL | SL_STYLE_BACKG_FILL | SL_STYLE_OUTLINE|
              SL_STYLE_TEXT | SL_STYLE_BACKG_TEXT),
              ClrGray, ClrBlack, ClrSilver, ClrWhite, ClrWhite,
              g_psFontCmss16b, "0", 0, 0, OnTempSliderChange);

Slider(g_sCurrentSlider, &g_sBackground, 0, 0,
             &g_sKentec320x240x16_SSD2119, DEFAULT_SLIDER_X, 146, DEFAULT_SLIDER_WIDTH, 30, 0, 5000, 5000,
             (SL_STYLE_FILL | SL_STYLE_BACKG_FILL | SL_STYLE_OUTLINE|
              SL_STYLE_TEXT | SL_STYLE_BACKG_TEXT),
              ClrGray, ClrBlack, ClrSilver, ClrWhite, ClrWhite,
              g_psFontCmss16b, "0", 0, 0, OnCurrentSliderChange);

// the button that changes the page
RectangularButton(g_ChangePageBttn, WIDGET_ROOT, 0, 0,
                  &g_sKentec320x240x16_SSD2119, 290, 0, 30, 30,
                  (PB_STYLE_OUTLINE | PB_STYLE_TEXT_OPAQUE | PB_STYLE_TEXT |
                     PB_STYLE_FILL | PB_STYLE_RELEASE_NOTIFY),
                     ClrDarkBlue, ClrBlue, ClrWhite, ClrWhite,
                     g_psFontCmss16b, "+", 0, 0, 0, 0, PageChangePress);





// the graphing section
Canvas(g_sGraphBackground, 0, 0, &g_sGraphSelectBttnFwd,
       &g_sKentec320x240x16_SSD2119, 0, 31, 320, 208,
       CANVAS_STYLE_APP_DRAWN, 0, 0, 0, 0, 0, 0, GraphDisplayUpdate);

// button that lets them change what is being graphed
RectangularButton(g_sGraphSelectBttnFwd, &g_sGraphBackground, &g_sGraphSelectBttnBack, 0,
                  &g_sKentec320x240x16_SSD2119, 180, 210, 100, 25,
                  (PB_STYLE_OUTLINE | PB_STYLE_TEXT_OPAQUE | PB_STYLE_TEXT |
                   PB_STYLE_FILL | PB_STYLE_RELEASE_NOTIFY),
                   ClrDarkBlue, ClrBlue, ClrWhite, ClrWhite,
                   g_psFontCmss16b, "Next", 0, 0, 0, 0, GraphChangePressFwd);

// button that lets them change what is being graphed
RectangularButton(g_sGraphSelectBttnBack, &g_sGraphBackground, 0, 0,
                  &g_sKentec320x240x16_SSD2119, 40, 210, 100, 25,
                  (PB_STYLE_OUTLINE | PB_STYLE_TEXT_OPAQUE | PB_STYLE_TEXT |
                   PB_STYLE_FILL | PB_STYLE_RELEASE_NOTIFY),
                   ClrDarkBlue, ClrBlue, ClrWhite, ClrWhite,
                   g_psFontCmss16b, "Prev", 0, 0, 0, 0, GraphChangePressBack);


void GraphChangePressFwd(tWidget *psWidget) {
    selectedGraph = (selectedGraph + 1) % NUMBER_OF_GRAPHS;
    WidgetPaint((tWidget *)WIDGET_ROOT);
}

void GraphChangePressBack(tWidget *psWidget) {
    selectedGraph = (selectedGraph + NUMBER_OF_GRAPHS - 1) % NUMBER_OF_GRAPHS;
    WidgetPaint((tWidget *)WIDGET_ROOT);
}

void PageChangePress(tWidget *psWidget) {
    if (page == 0) {
        PushButtonTextSet((tPushButtonWidget *)psWidget, "-");
        WidgetPaint((tWidget *)psWidget);
        WidgetRemove((tWidget *)&g_sBackground);
        WidgetAdd(WIDGET_ROOT, (tWidget *)&g_sGraphBackground);
        WidgetPaint((tWidget *)WIDGET_ROOT);
        page = 1;
    } else {
        PushButtonTextSet((tPushButtonWidget *)psWidget, "+");
        WidgetPaint((tWidget *)psWidget);
        WidgetAdd(WIDGET_ROOT, (tWidget *)&g_sBackground);
        WidgetRemove((tWidget *)&g_sGraphBackground);
        WidgetPaint((tWidget *)WIDGET_ROOT);
        page = 0;
    }
}

void StartStopBttnPress(tWidget *psWidget)
{
    motorStartStop = !motorStartStop;

    if(motorStartStop)
    {
        //
        // Change the button text to indicate the new function.
        //
        PushButtonTextSet(&g_sStartStopBttn, "Stop");
        setEnable(1);
        setTargetRPM(g_sRPMSlider.i32Value);

        //
        // Repaint the pushbutton and all widgets beneath it (in this case,
        // the welcome message).
        //
        WidgetPaint((tWidget *)&g_sStartStopBttn);
    }
    else
    {
        //
        // Change the button text to indicate the new function.
        //
        PushButtonTextSet(&g_sStartStopBttn, "Start");
        setStoppingState(1);

        WidgetPaint((tWidget *)&g_sStartStopBttn);
    }
}

void ReverseForwardBttnPress(tWidget *psWidget)
{

    if (getEnable())
    {

    }
    else
    {
        motorReversed = !motorReversed;
            setReverse(motorReversed);

            if(motorReversed)
            {
                //
                // Change the button text to indicate the new function.
                //
                PushButtonTextSet((tPushButtonWidget *)psWidget, "Forward");

                //
                // Repaint the pushbutton and all widgets beneath it (in this case,
                // the welcome message).
                //
            }
            else
            {
                PushButtonTextSet((tPushButtonWidget *)psWidget, "Reverse");
            }
            WidgetPaint(psWidget);
    }

}

void OnRPMSliderChange(tWidget* psWidget, int32_t i32Value)
{
    setTargetRPM(i32Value);
    static char pcCanvasText[10];
    static char pcSliderText[10];

    usprintf(pcCanvasText, "%d RPM", i32Value);
    CanvasTextSet(&g_sBackground, pcCanvasText);
    //WidgetPaint((tWidget*)&g_sBackground);
    SliderValueSet((tSliderWidget*)psWidget, i32Value);
    //WidgetPaint((tWidget*)psWidget);


    usprintf(pcSliderText, "%d RPM", i32Value);
    SliderTextSet((tSliderWidget*)psWidget, pcSliderText);
    WidgetPaint((tWidget*)psWidget);
}

void OnAccelSliderChange(tWidget* psWidget, int32_t i32Value)
{
    volatile uint8_t scaledAccel = i32Value / 7.84f;
    set_anymotionThreshold(scaledAccel);
    static char pcCanvasText[10];
    static char pcSliderText[10];

    usprintf(pcCanvasText, "%d mG", i32Value);
    CanvasTextSet(&g_sBackground, pcCanvasText);
    //WidgetPaint((tWidget*)&g_sBackground);
    SliderValueSet((tSliderWidget*)psWidget, i32Value);
    //WidgetPaint((tWidget*)psWidget);


    usprintf(pcSliderText, "%d mG", i32Value);
    SliderTextSet((tSliderWidget*)psWidget, pcSliderText);
    WidgetPaint((tWidget*)psWidget);
}

void OnTempSliderChange(tWidget* psWidget, int32_t i32Value)
{
    set_temperatureAllowed((float)i32Value);
    static char pcCanvasText[10];
    static char pcSliderText[10];

    usprintf(pcCanvasText, "%d Deg", i32Value);
    CanvasTextSet(&g_sBackground, pcCanvasText);
    //WidgetPaint((tWidget*)&g_sBackground);
    SliderValueSet((tSliderWidget*)psWidget, i32Value);
    //WidgetPaint((tWidget*)psWidget);


    usprintf(pcSliderText, "%d Deg", i32Value);
    SliderTextSet((tSliderWidget*)psWidget, pcSliderText);
    WidgetPaint((tWidget*)psWidget);
}

void OnCurrentSliderChange(tWidget* psWidget, int32_t i32Value)
{
    //SET THE ALLOWABLE CURRENT WITH A FUNCTION CALL

    setCurrentThreshold(i32Value);

    static char pcCanvasText[10];
    static char pcSliderText[10];

    usprintf(pcCanvasText, "%d mA", i32Value);
    CanvasTextSet(&g_sBackground, pcCanvasText);
    //WidgetPaint((tWidget*)&g_sBackground);
    SliderValueSet((tSliderWidget*)psWidget, i32Value);
    //WidgetPaint((tWidget*)psWidget);


    usprintf(pcSliderText, "%d mA", i32Value);
    SliderTextSet((tSliderWidget*)psWidget, pcSliderText);
    WidgetPaint((tWidget*)psWidget);
}

void set_theDay(uint8_t the_day) {
    GPIO_write(Board_LED2, !the_day);
    if (day != the_day) {
        dayChangedFlag = 1;
    }
    day = the_day;
}



void TopDisplayUpdate() {
    GPIO_toggle(Board_LED3);
    GrContextFontSet(&sContext, g_psFontCmss20b);

    char newString[40];

    // Draw the new string.
    GrContextForegroundSet(&sContext, ClrWhite);

    DateTimeDisplayGet(newString, sizeof(newString));

    // Draw a black rectangle.
    tRectangle sRect;
    GrContextForegroundSet(&sContext, ClrBlack);
    sRect.i16XMin = 0;
    sRect.i16YMin = 0;
    sRect.i16XMax = 280;
    sRect.i16YMax = 29;
    GrRectFill(&sContext, &sRect);

    GrContextForegroundSet(&sContext, ClrWhite);

    //FrameDraw(&sContext, newString);
    GrStringDraw(&sContext, newString, -1, 3, 5, 0);
}

void RPMDisplayUpdate()
{
    if (abs(motorSpeedDisplay - getCurrentRPM()) > 10)
    {
        motorSpeedDisplay = getCurrentRPM();
        static char RPMText[10];
        usprintf(RPMText, "%d RPM", motorSpeedDisplay);
        PushButtonTextSet(&g_sRPMLabel, RPMText);
        if (page == 0) {
            WidgetPaint((tWidget*)&g_sRPMLabel);
        }
    }
}

void BackgroundColourDisplayUpdate() {
    int redraw = 0;
    if (dayChangedFlag) {
        dayChangedFlag = 0;
        if (day) {
            g_sBackground.ui32FillColor = ClrLemonChiffon;
        } else {
            g_sBackground.ui32FillColor = ClrMidnightBlue;
        }
        redraw = 1;
    }
    if (estopDisplay != getEstop()) {
        estopDisplay = getEstop();
        if (getEstop()) {
            g_sBackground.ui32FillColor = ClrOrangeRed;
        } else {
            if (day) {
                g_sBackground.ui32FillColor = ClrLemonChiffon;
            } else {
                g_sBackground.ui32FillColor = ClrMidnightBlue;
            }
        }
        redraw = 1;
    }
    if (redraw && page == 0) {
        WidgetPaint((tWidget*)&g_sBackground);
    }

}


#define GRAPH_TOP 57
#define GRAPH_LEFT 43
#define GRAPH_BOTTOM 190
#define GRAPH_RIGHT 300


void GraphDisplayUpdate(tWidget *psWidget, tContext *psContext) {
    // WidgetPaint((tWidget*)&g_sGraphBackground);
    // first find the min and max values.
    volatile float min;
    volatile float max;
    volatile int i = 0;
    min = FLT_MAX;
    max = FLT_MIN;
    for (i = 0; i < BUFFER_LENGTH; i++) {
        volatile float v = get_bufferValue(selectedGraph, i);
        if (v > max) {
            max = v;
        }
        if (v < min) {
            min = v;
        }
    }
    max *= 1.1;
    min *= 0.9;
    if (selectedGraph == 3) {
        // accelerometer looks nicer with set limits
        min = 0.0f;
        max = 2.0f;
    }
    // draw a big rectangle
    tRectangle sRect;
    GrContextForegroundSet(psContext, ClrMidnightBlue);
    sRect.i16XMin = 0;
    sRect.i16YMin = 6;
    sRect.i16XMax = 320;
    sRect.i16YMax = 240;
    GrRectFill(psContext, &sRect);

    // draw two lines
    GrContextForegroundSet(psContext, ClrWhite);
    GrLineDraw(psContext, GRAPH_LEFT, GRAPH_TOP, GRAPH_LEFT, GRAPH_BOTTOM);
    GrLineDraw(psContext, GRAPH_LEFT, GRAPH_BOTTOM, GRAPH_RIGHT, GRAPH_BOTTOM);

    // add some labels
    // add some labels
    char labelBuffer[20];
    System_sprintf(labelBuffer, "%.2f", min);

    GrContextFontSet(psContext, &g_sFontCmss12b);
    GrStringDraw(psContext, labelBuffer, -1, GRAPH_LEFT-40, GRAPH_BOTTOM-5, 0);

    System_sprintf(labelBuffer, "%.2f", max);

    GrStringDraw(psContext, labelBuffer, -1, GRAPH_LEFT-40, GRAPH_TOP-5, 0);

    // draw the points.
    float dist = (GRAPH_RIGHT - GRAPH_LEFT) / (float)BUFFER_LENGTH;
    // gradient of the function that converts max - min to GRAPH_TOP to GRAPH_BOTTOM
    float m = (GRAPH_TOP - GRAPH_BOTTOM) / (max - min);
    int old_x;
    int old_y;
    for (i = 0; i < BUFFER_LENGTH; i++) {
        // real y value.
        float ry = get_bufferValue(selectedGraph, i);
        // graphical y value.
        float y = (m * ry) - (m * min) + GRAPH_BOTTOM;
        // graphical x value.
        float x = GRAPH_LEFT + dist * i;
        if (i > 0) {
            // draw a line.
            GrLineDraw(psContext, old_x, old_y, (int)x, (int)y);
        }
        GrCircleDraw(psContext, (int)x, (int)y, 2);
        old_x = (int)x;
        old_y = (int)y;
    }

    // draw the label
    GrContextForegroundSet(psContext, ClrWhite);
    GrContextFontSet(psContext, g_psFontCmss16b);
    GrStringDraw(psContext, messages[selectedGraph], -1, 3, 35, 0);

}

volatile uint8_t ui8Image;

void gui_init(void) {
    //
    // Initialize the display driver.
    //
    Kentec320x240x16_SSD2119Init(freq.lo);

    //
    // Initialize the graphics context.
    //

    GrContextInit(&sContext, &g_sKentec320x240x16_SSD2119);
    TouchScreenInit(freq.lo);
    TouchScreenCallbackSet(WidgetPointerMessage);


    GrContextFontSet(&sContext, &g_sFontCm20);
    GrContextForegroundSet(&sContext, ClrWhite);

}

void guiTaskFxn(UArg arg0, UArg arg1) {
    estopDisplay = getEstop();
    dayChangedFlag = 1;
    page = 0;
    selectedGraph = 0;
    clock_lastTime = Clock_getTicks();

    // Add the compile-time defined widgets to the widget tree.
    WidgetAdd(WIDGET_ROOT, (tWidget *)&g_sBackground);

    OnRPMSliderChange((tWidget*)&g_sRPMSlider, 300);
    OnAccelSliderChange((tWidget*)&g_sAccelSlider, 1200);
    OnTempSliderChange((tWidget*)&g_sTempSlider, 40);
    OnCurrentSliderChange((tWidget*)&g_sCurrentSlider, 5000);

    // Paint the widget tree to make sure they all appear on the display.
    WidgetPaint(WIDGET_ROOT);


    while (1) {
        SysCtlDelay(100);

        if (Clock_getTicks() - clock_lastTime > 10000) {
            TopDisplayUpdate();
            clock_lastTime = Clock_getTicks();
        }

        RPMDisplayUpdate();
        BackgroundColourDisplayUpdate();



        if (page == 1) {
            if (is_updated(selectedGraph)) {
                WidgetPaint(WIDGET_ROOT);
            }
        }
        WidgetMessageQueueProcess();
    }
}

