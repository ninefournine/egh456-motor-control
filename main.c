/*
 * Copyright (c) 2015, Texas Instruments Incorporated
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * *  Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * *  Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * *  Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/*
 *  ======== empty.c ========
 */

/* XDCtools Header files */
#include <xdc/std.h>
#include <xdc/runtime/System.h>
#include <math.h>
#include <time.h>

/* BIOS Header files */
#include <ti/sysbios/BIOS.h>
#include <ti/sysbios/knl/Task.h>
#include <ti/sysbios/knl/Clock.h>
#include <ti/sysbios/knl/Swi.h>
#include <ti/sysbios/hal/Timer.h>
#include <ti/sysbios/knl/Event.h>

#include <xdc/runtime/Error.h>

#include <ti/sysbios/family/arm/m3/Hwi.h>

/* TI-RTOS Header files */
// #include <ti/drivers/EMAC.h>
#include <ti/drivers/GPIO.h>
// #include <ti/drivers/I2C.h>
// #include <ti/drivers/SDSPI.h>
// #include <ti/drivers/SPI.h>
#include <ti/drivers/UART.h>
#include <ti/drivers/PWM.h>
// #include <ti/drivers/USBMSCHFatFs.h>
// #include <ti/drivers/Watchdog.h>
// #include <ti/drivers/WiFi.h>
#include "utils/ustdlib.h"
#include <string.h>
#include <xdc/runtime/Types.h>
#include <stdbool.h>
#include "driverlib/sysctl.h"

#include "inc/hw_memmap.h"
#include "inc/hw_ints.h"
#include "driverlib/gpio.h"
#include "driverlib/interrupt.h"
#include "driverlib/pin_map.h"
#include "driverlib/uart.h"
#include "driverlib/i2c.h"
#include "driverlib/adc.h"
#include "driverlib/hibernate.h"

#include "grlib/grlib.h"
#include "grlib/widget.h"
#include "grlib/canvas.h"
#include "grlib/pushbutton.h"


#include "drivers/frame.h"
#include "drivers/kentec320x240x16_ssd2119_spi.h"
#include "drivers/pinout.h"
#include "drivers/touch.h"

/* Board Header file */
#include "Board.h"

#include "temperature.h"
#include "boost_sensors.h"
#include "gui.h"
#include "motor.h"
#include "ring_buffer.h"
#include "calendar.h"
#include "current.h"

#define TASKSTACKSIZE   1024

Task_Struct task0Struct;
Char task0Stack[TASKSTACKSIZE];

/* frequency object */
Types_FreqHz freq;

/* console UART objects */
char UARTBuffer[40];
volatile uint8_t reportFlag;

volatile uint8_t motorProcessCounter;
volatile uint16_t rpmSampleCounter;



/* clock object for the 1ms interval */
Clock_Params clockParams;
Clock_Handle myClock;
Error_Block eb;


/* Event object for the sensors? */
// NB: only one task can pend on an event at a time!!
// so we might have to make another event for motor state machine --> GUI ?
Event_Handle sensorEvent;


// task for the graphics
Task_Struct graphicsStruct;
Char graphicsStack[TASKSTACKSIZE];

/***
 * ADC STUFF STARTS HERE
 */

/*
 * ADC STUFF ENDS HERE
 */

/*
 * A 1ms period function used to do lots of things.
 */
// THIS is the 1ms function!!!!!!
void clockFxn(UArg arg1) {

    temperature_process();

    sensors_process();


    motorProcessCounter++;

    if (motorProcessCounter >= 10) {
        motorProcessCounter = 0;

        motor_process();
    }

    rpmSampleCounter++;

    // every 200ms sample
    if (rpmSampleCounter >= 200) {

        rpmSampleCounter = 0;
        insert_bufferValue(4, (float)getCurrentRPM());
    }

    adc_process();

    // read the current RPM and do some acceleration thing i guess
    //GPIO_toggle(Board_LED0);
    return;
}


/*
 *  ======== heartBeatFxn ========
 *  Toggle the Board_LED0. The Task_sleep is determined by arg0 which
 *  is configured for the heartBeat Task instance.
 */
Void heartBeatFxn(UArg arg0, UArg arg1)
{
    set_anymotionThreshold(210);

    while (1) {
        Task_sleep((unsigned int)arg0);
        //Event_pend(sensorEvent, Event_Id_NONE, (Event_Id_00), BIOS_WAIT_FOREVER);
        //Event_pend(sensorEvent, Event_Id_NONE, (Event_Id_02), BIOS_WAIT_FOREVER);
        int event = Event_pend(sensorEvent, Event_Id_NONE, (Event_Id_00 + Event_Id_02 + Event_Id_03), BIOS_WAIT_FOREVER);

        //int left = (int)currentRPM;
        //int temp = (int)(currentRPM * 10);
        //int right = temp % 10;
        //usprintf(UARTBuffer, "RPM: %d.%d\r\n", left, right);
        //UARTSend(UART0_BASE, UARTBuffer, strlen(UARTBuffer));

        if (event & Event_Id_00) {
            float tempB = temperature_getAmbient();
            float tempA = temperature_getMotor();
            insert_bufferValue(1, tempA);
            insert_bufferValue(0, tempB);
            System_sprintf(UARTBuffer, "%f\r\n", tempB);
            UARTSend(UART0_BASE, (uint8_t*)UARTBuffer, strlen(UARTBuffer));
            if (tempA > get_temperatureAllowed() || tempB > get_temperatureAllowed()) {
                setEstop(1);
            }
        }

        if (event & Event_Id_02) {
            float currentLux = get_currentLux();
            insert_bufferValue(2, currentLux);
            if (currentLux > 5) {
                set_theDay(1);
            } else {
                set_theDay(0);
            }
        }

        if (event & Event_Id_03) {
            float mag = get_currentMagnitude();
            insert_bufferValue(3, mag);
        }

        //TEMP - Event_Id_00
//        float tempA = temperature_getAmbient();
//        float tempB = temperature_getMotor();
//        System_sprintf(UARTBuffer, "%f %f\r\n", tempA, tempB);
//        UARTSend(UART0_BASE, (uint8_t*)UARTBuffer, strlen(UARTBuffer));

        // LUX - Event_id_02
//        float currentLux = get_currentLux();
//        System_sprintf(UARTBuffer, "%f\r\n", currentLux);
//        UARTSend(UART0_BASE, (uint8_t*)UARTBuffer, strlen(UARTBuffer));

        // ACCLE - event_id_03
//        float x, y, z;
//        get_gForce(&x, &y, &z);
//
//        System_sprintf(UARTBuffer, "%f %f\r\n", z, x);
//        UARTSend(UART0_BASE, (uint8_t*)UARTBuffer, strlen(UARTBuffer));

        // ACCLE2 - event_id_03
//        float mag = get_currentMagnitude();
//        System_sprintf(UARTBuffer, "%f\r\n", mag);
//        UARTSend(UART0_BASE, (uint8_t*)UARTBuffer, strlen(UARTBuffer));
        int currentRPM = getCurrentRPM();
        int targetRPM = getTargetRPM();
        int left = (int)currentRPM;
        int temp = (int)(currentRPM * 10);
        int right = temp % 10;
        usprintf(UARTBuffer, "%d %d.%d %d\r\n", (int)targetRPM, left, right, getCurrentDuty());
        UARTSend(UART0_BASE, UARTBuffer, strlen(UARTBuffer));

    }
}


void AnyMotionCallback(unsigned int index) {
    GPIO_clearInt(BMI160_INT1);

    UARTSend(UART0_BASE, "Interrupt!\r\n", 12);

    setEstop(1);
}



/*
 *  ======== main ========
 */
int main(void)
{

    BIOS_getCpuFreq(&freq);

    /* Call board init functions */
    Board_initGeneral();
    // Board_initEMAC();
    Board_initGPIO();
    Board_initPWM();
    //Board_initI2C();
    // Board_initSDSPI();
    // Board_initSPI();
    // Board_initUART();
    // Board_initUSB(Board_USBDEVICE);
    // Board_initUSBMSCHFatFs();
    // Board_initWatchdog();
    // Board_initWiFi();
//

    /* Construct heartBeat Task  thread */
    Task_Params taskParams;
    Task_Params_init(&taskParams);
    taskParams.arg0 = 100;
    taskParams.stackSize = TASKSTACKSIZE;
    taskParams.stack = &task0Stack;
    taskParams.priority = 3;
    Task_construct(&task0Struct, (Task_FuncPtr)heartBeatFxn, &taskParams, NULL);

    /* Other task for graphics*/
    Task_Params graphicstaskParams;
    Task_Params_init(&graphicstaskParams);
    graphicstaskParams.arg0 = 100;
    graphicstaskParams.stackSize = TASKSTACKSIZE;
    graphicstaskParams.stack = &graphicsStack;
    graphicstaskParams.priority = 1;
    Task_construct(&graphicsStruct, (Task_FuncPtr)guiTaskFxn, &graphicstaskParams, NULL);


    motorProcessCounter = 0;
    rpmSampleCounter = 0;

    // clock SWI for various purposes.
    Error_init(&eb);
    Clock_Params_init(&clockParams);
    clockParams.period = 10; // 1ms timer.
    clockParams.startFlag = TRUE;
    clockParams.arg = (UArg)0x5555;
    myClock = Clock_create(clockFxn, 100, &clockParams, &eb);
    if (myClock == NULL) {
       System_abort("Clock create failed");
    }

    /* events */
    sensorEvent = Event_create(NULL, NULL);

    temperature_init(sensorEvent, Event_Id_00);

    // i2c
    sensors_init(sensorEvent, Event_Id_02, Event_Id_03);

    UARTSend(UART0_BASE, "Hello World\r\n", 13);


    motor_init();
    adc_init();

    buffers_init();

    calendar_init();



    /* Set up a different interrupt for the anymotion interrupt. */
    GPIO_setCallback(BMI160_INT1, AnyMotionCallback);
    GPIO_enableInt(BMI160_INT1);

    gui_init();

     /* Turn on user LED */
    GPIO_write(Board_LED0, Board_LED_ON);


    System_printf("Starting the example\nSystem provider is set to SysMin. "
                  "Halt the target to view any SysMin contents in ROV.\n");
    /* SysMin will only print to the console when you call flush or exit */
    System_flush();

    /* Start BIOS */
    BIOS_start();

    return (0);
}
