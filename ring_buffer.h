/*
 * ring_buffer.h
 *
 *  Created on: 3 Jun. 2019
 *      Author: caoj7
 */

#ifndef RING_BUFFER_H_
#define RING_BUFFER_H_


#define BUFFER_LENGTH 30

void buffers_init();
float get_bufferValue(int bufferID, int i);
void insert_bufferValue(int bufferID, float value);

uint8_t is_updated(int bufferID);

#endif /* RING_BUFFER_H_ */
