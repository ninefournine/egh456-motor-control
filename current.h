/*
 * current.h
 *
 *  Created on: 5 Jun. 2019
 *      Author: caoj7
 */

#ifndef CURRENT_H_
#define CURRENT_H_

void ADCInterrupt(void);
void adc_process(void);
void adc_init(void);
void setCurrentThreshold(uint32_t newThreshold);
uint32_t getCurrentThreshold(void);

#endif /* CURRENT_H_ */
