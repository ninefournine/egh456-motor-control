/*
 * boost_sensors.c
 *
 *  Created on: 28 May 2019
 *      Author: caoj7
 */

#include <stdint.h>
#include <stdbool.h>
#include <math.h>

#include <ti/sysbios/knl/Event.h>
#include <ti/drivers/GPIO.h>

#include "driverlib/i2c.h"
#include "driverlib/sysctl.h"
#include "driverlib/gpio.h"
#include "driverlib/pin_map.h"
#include "driverlib/interrupt.h"

#include "inc/hw_memmap.h"
#include "inc/hw_ints.h"

#include "Board.h"
#include "temperature.h"
#include "boost_sensors.h"

/*
 * State machine for managing a single I2C transaction.
 */
enum i2c_processStateMachine {
    I2C_IDLE = 0,
    I2C_START_RECEIVE,
    I2C_READ_RECEIVE,
    I2C_START_SEND
};

enum i2c_processStateMachine i2c_currentProcessState;

/*
 * State machine for managing the various sensors.
 */
enum i2c_sensorStateMachine {
    I2C_VERIFY_OPT3001 = 0,
    I2C_WAITING,
    I2C_VERIFICATION_DONE,
    I2C_BMI160_CHECK_DONE,
    I2C_OPT3001_SETUP_CHECK,
    I2C_OPT3001_SETUP_CHECK_COMPLETE,
    I2C_OPT3001_SETUP_COMPLETE,
    I2C_OPT3001_READ,
    I2C_OPT3001_READ_COMPLETE,
    I2C_BMI160_RESET_CMD,
    I2C_BMI160_RESET_CMD_WAIT,
    I2C_BMI160_BOOT_CMD,
    I2C_BMI160_BOOT_CMD_WAIT,
    I2C_BMI160_BOOT_CMD_CHECK,
    I2C_BMI160_BOOT_CMD_COMPLETE,
    I2C_BMI160_CONFIG_CHECK,
    I2C_BMI160_CONF_CMD_CHECK_A,
    I2C_BMI160_CONF_CMD_CHECK_B,
    I2C_BMI160_CONF_CMD_CHECK_C,
    I2C_BMI160_CONF_CMD_CHECK_D,
    I2C_BMI160_CONF_CMD_CHECK_E,
    I2C_BMI160_CONF_CMD_CHECK_F,
    I2C_BMI160_CONF_CMD_CHECK_G,
    I2C_BMI160_CONF_CMD_CHECK_H,
    I2C_BMI160_CONF_CMD_CHECK_I,
    I2C_BMI160_CONF_CMD_COMPLETE,
    I2C_BMI160_CONF_ANYMOTION,
    I2C_BMI160_READ
};

enum i2c_mode {
    I2C_SEND = 0,
    I2C_RECV
};

enum i2c_sensorStateMachine i2c_currentState;
enum i2c_sensorStateMachine i2c_nextState;
enum i2c_mode i2c_currentMode;


/*
 * Parameters for an i2c transaction.
 */
uint8_t i2c_slaveAddress;
uint8_t i2c_targetRegister;
uint16_t i2c_delayCounter;
int numberOfBytes;
int currentByte;

/*
 * Receive and send buffers for i2c.
 */
char recvBuffer[19];
char sendBuffer[10];



/*
 * Variables that get populated by the i2c stuff
 */
volatile float gForceX;
volatile float gForceY;
volatile float gForceZ;

#define NUMBER_OF_LUXES 5
volatile float currentLuxes[NUMBER_OF_LUXES];
volatile uint8_t currentLuxPos;

#define NUMBER_OF_MAGNITUDES 30
volatile float currentMagnitudes[NUMBER_OF_MAGNITUDES];
volatile uint8_t currentMagnitudePos;


/*
 * Interrupt things
 */
volatile uint8_t anymotionThreshold = 2;
volatile uint8_t anymotionThresholdChanged = 0;

/*
 * Event handles.
 */
Event_Handle boostEvt;
unsigned int lightMask;
unsigned int speedMask;







void sensors_init(Event_Handle evt, unsigned int lightMaskIn, unsigned int speedMaskIn) {
    lightMask = lightMaskIn;
    speedMask = speedMaskIn;
    boostEvt = evt;
    i2c_currentState = I2C_VERIFY_OPT3001;

    currentLuxPos = 0;
    currentMagnitudePos = 0;

    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPION);

    // Configure pins for I2C Master Interface
    GPIOPinConfigure(GPIO_PN5_I2C2SCL);
    GPIOPinConfigure(GPIO_PN4_I2C2SDA);
    GPIOPinTypeI2C(GPIO_PORTN_BASE, GPIO_PIN_4);
    GPIOPinTypeI2CSCL(GPIO_PORTN_BASE, GPIO_PIN_5);

    // Stop the clock, Reset, and Enable I2C module
    SysCtlPeripheralDisable(SYSCTL_PERIPH_I2C2);
    SysCtlPeripheralReset(SYSCTL_PERIPH_I2C2);
    SysCtlPeripheralEnable(SYSCTL_PERIPH_I2C2);

    // Wait for the Peripheral to be ready for programming
    //
    while(!SysCtlPeripheralReady(SYSCTL_PERIPH_I2C2));

    // Initialize and Configure the Master Module
    //
    I2CMasterInitExpClk(I2C2_BASE, freq.lo, true);

    I2CMasterGlitchFilterConfigSet(I2C2_BASE, I2C_MASTER_GLITCH_FILTER_8);

    // Enable Interrupts for Arbitration Lost, Stop, NAK, Clock Low
    // Timeout and Data.
    //
    I2CMasterIntEnableEx(I2C2_BASE, (I2C_MASTER_INT_ARB_LOST |
    I2C_MASTER_INT_STOP | I2C_MASTER_INT_NACK |
    I2C_MASTER_INT_TIMEOUT | I2C_MASTER_INT_DATA));

    IntEnable(INT_I2C2);
}


void I2CInterrupt(void) {
    uint32_t ui32I2CMasterInterruptStatus;

    //
    // Get the masked interrupt status and clear the flags
    //
    ui32I2CMasterInterruptStatus = I2CMasterIntStatusEx(I2C2_BASE, true);
    I2CMasterIntClearEx(I2C2_BASE, ui32I2CMasterInterruptStatus);

    switch (i2c_currentProcessState) {
        case I2C_IDLE: {
            if (i2c_currentMode == I2C_RECV) {
                i2c_currentProcessState = I2C_START_RECEIVE;
            } else {
                i2c_currentProcessState = I2C_START_SEND;
            }
            // Send the slave address and register we want to read/write to.
            I2CMasterSlaveAddrSet(I2C2_BASE, i2c_slaveAddress, false);
            I2CMasterDataPut(I2C2_BASE, i2c_targetRegister);
            // Don't send the STOP bit.
            I2CMasterControl(I2C2_BASE, I2C_MASTER_CMD_BURST_SEND_START);
            break;
        }
        case I2C_START_RECEIVE: {
            // If the address was NAK'ed then go to stop state
            // TODO: error handling here?
            if(ui32I2CMasterInterruptStatus & I2C_MASTER_INT_NACK)
            {
                i2c_currentProcessState = I2C_IDLE;
            }
            // Start a receive request.
            I2CMasterSlaveAddrSet(I2C2_BASE, i2c_slaveAddress, true);
            // if there's only one byte to read...
            if (currentByte == (numberOfBytes - 1)) {
                I2CMasterControl(I2C2_BASE, I2C_MASTER_CMD_SINGLE_RECEIVE);
            } else {
                I2CMasterControl(I2C2_BASE, I2C_MASTER_CMD_BURST_RECEIVE_START);
            }
            i2c_currentProcessState = I2C_READ_RECEIVE;
            break;
        }
        case I2C_READ_RECEIVE: {
            if (ui32I2CMasterInterruptStatus & I2C_MASTER_INT_DATA) {
                recvBuffer[currentByte] = I2CMasterDataGet(I2C2_BASE);

                currentByte++;

                if (currentByte == numberOfBytes) {
                    i2c_currentProcessState = I2C_IDLE;
                    i2c_currentState = i2c_nextState;
                    break; // don't send another I2C_MASTER_CMD_BURST_RECEIVE_CONT etc.
                }

                if (currentByte == numberOfBytes - 1) {
                    // only one more byte to go!
                    I2CMasterControl(I2C2_BASE, I2C_MASTER_CMD_BURST_RECEIVE_FINISH);
                }
                else {
                    I2CMasterControl(I2C2_BASE, I2C_MASTER_CMD_BURST_RECEIVE_CONT);
                }
            }
            break;
        }
        case I2C_START_SEND: {
            if (ui32I2CMasterInterruptStatus & I2C_MASTER_INT_STOP) {
                // we're done.
                i2c_currentProcessState = I2C_IDLE;
                i2c_currentState = i2c_nextState;
                break; // don't send another I2C_MASTER_CMD_BURST_START_CONT etc.
            }

            I2CMasterDataPut(I2C2_BASE, sendBuffer[currentByte]);
            // that was the last byte!
            if (currentByte == numberOfBytes - 1) {
                I2CMasterControl(I2C2_BASE, I2C_MASTER_CMD_BURST_SEND_FINISH);
            } else {
                I2CMasterControl(I2C2_BASE, I2C_MASTER_CMD_BURST_SEND_CONT);

            }
            currentByte++;
            break;
        }
    }
}



void SensorOpt3001Convert(uint8_t high, uint8_t low, float *convertedLux)
{
    uint16_t e, m;

    m = (high << 8 | low) & 0x0FFF;
    e = high >> 4;

    *convertedLux = m * (0.01 * exp2(e));
}

void SensorBmi160Convert(uint8_t high, uint8_t low, float *convertedG)
{
    int16_t compliment = (int16_t)((high << 8) | low);
    *convertedG = (float)compliment / 8192.0f;
}


void sensors_process(void) {
    switch (i2c_currentState) {
        case I2C_VERIFY_OPT3001: {
            // Specify the parameters for this transaction.
            // for the sake of avoiding bugs we'll always explicitly state each parameter.
            i2c_slaveAddress = 0x47;
            i2c_targetRegister = 0x7E;
            numberOfBytes = 2;
            currentByte = 0;
            i2c_currentMode = I2C_RECV;

            // manually fire the interrupt to start it off. when the interrupt is done
            // the state will change automatically to whatever we set i2c_nextState to
            i2c_currentState = I2C_WAITING;
            i2c_nextState = I2C_VERIFICATION_DONE;
            IntTrigger(INT_I2C2);
            break;
        }
        case I2C_VERIFICATION_DONE: {

            if (recvBuffer[0] == 'T' && recvBuffer[1] == 'I') {
                UARTSend(UART0_BASE, "OPT3001 OK\r\n", 12);
            }

            // let's try reading the BMI160's registers
            i2c_slaveAddress = 0x69;
            i2c_targetRegister = 0x00;
            numberOfBytes = 1;
            currentByte = 0;
            i2c_currentMode = I2C_RECV;

            i2c_currentState = I2C_WAITING;
            i2c_nextState = I2C_BMI160_CHECK_DONE;
            IntTrigger(INT_I2C2);

            break;
        }
        case I2C_BMI160_CHECK_DONE: {
            if (recvBuffer[0] == 0xD1) {
                UARTSend(UART0_BASE, "BMI160 OK\r\n", 11);
            }
            // do a write to the OPT3001
            i2c_slaveAddress = 0x47;
            i2c_targetRegister = 0x01;
            numberOfBytes = 2;
            currentByte = 0;
            i2c_currentMode = I2C_SEND;

            // the data, most significant byte first
            sendBuffer[0] = 0b11000100;
            sendBuffer[1] = 0b00010000;

            i2c_currentState = I2C_WAITING;
            i2c_nextState = I2C_OPT3001_SETUP_CHECK;
            i2c_delayCounter = 0;
            IntTrigger(INT_I2C2);
            break;
        }
        case I2C_OPT3001_SETUP_CHECK: {
            // Check if we've written to the configuration register.
            i2c_slaveAddress = 0x47;
            i2c_targetRegister = 0x01;
            numberOfBytes = 2;
            currentByte = 0;
            i2c_currentMode = I2C_RECV;

            i2c_currentState = I2C_WAITING;
            i2c_nextState = I2C_OPT3001_SETUP_CHECK_COMPLETE;
            IntTrigger(INT_I2C2);
            break;
        }
        case I2C_OPT3001_SETUP_CHECK_COMPLETE: {
            // check the values are what we expected.
            if (recvBuffer[0] == 0b11000100 && recvBuffer[1] == 0b00010000) {
                UARTSend(UART0_BASE, "3001 CF OK\r\n", 12);
                i2c_currentState = I2C_BMI160_RESET_CMD;
            } else {
                i2c_currentState = I2C_BMI160_CHECK_DONE;
            }
            break;
        }
        case I2C_BMI160_RESET_CMD: {
            // write values to the CMD register to softreset the device.
            // do a write to the BMI160
            i2c_slaveAddress = 0x69;
            i2c_targetRegister = 0x7E;
            numberOfBytes = 1;
            currentByte = 0;
            i2c_currentMode = I2C_SEND;

            sendBuffer[0] = 0xB6; // boot the accelerometer into Normal Mode

            i2c_currentState = I2C_WAITING;
            i2c_nextState = I2C_BMI160_RESET_CMD_WAIT;
            i2c_delayCounter = 0;
            IntTrigger(INT_I2C2);

            break;
        }
        case I2C_BMI160_RESET_CMD_WAIT: {
            // wait 200ms.
            i2c_delayCounter++;
            if (i2c_delayCounter > 200) {
                i2c_currentState = I2C_BMI160_BOOT_CMD;
            }
            break;
        }
        case I2C_BMI160_BOOT_CMD: {

            // write values to the CMD register to boot the device.
            // do a write to the BMI160
            i2c_slaveAddress = 0x69;
            i2c_targetRegister = 0x7E;
            numberOfBytes = 1;
            currentByte = 0;
            i2c_currentMode = I2C_SEND;

            sendBuffer[0] = 0x11; // boot the accelerometer into Normal Mode

            i2c_currentState = I2C_WAITING;
            i2c_nextState = I2C_BMI160_BOOT_CMD_WAIT;
            i2c_delayCounter = 0;
            IntTrigger(INT_I2C2);

            break;
        }
        case I2C_BMI160_BOOT_CMD_WAIT: {
            // wait 5ms (from datasheet)
            i2c_delayCounter++;
            if (i2c_delayCounter > 5) {
                i2c_delayCounter = 0;

                // grab the PMU register.
                i2c_slaveAddress = 0x69;
                i2c_targetRegister = 0x03;
                numberOfBytes = 1;
                currentByte = 0;
                i2c_currentMode = I2C_RECV;

                i2c_currentState = I2C_WAITING;
                i2c_nextState = I2C_BMI160_BOOT_CMD_CHECK;
                IntTrigger(INT_I2C2);
            }
            break;
        }
        case I2C_BMI160_BOOT_CMD_CHECK: {
            // check it's what we expected:
            if ((recvBuffer[0] & 0x11) > 0) {
                i2c_currentState = I2C_BMI160_BOOT_CMD_COMPLETE;
            } else {
                i2c_currentState = I2C_BMI160_BOOT_CMD;
            }
            break;
        }
        case I2C_BMI160_BOOT_CMD_COMPLETE: {
            // write to the ACC_CONF register.
            i2c_slaveAddress = 0x69;
            i2c_targetRegister = 0x40;
            numberOfBytes = 1;
            currentByte = 0;
            i2c_currentMode = I2C_SEND;

            sendBuffer[0] = 0b0101010; // configure to 800 Hz

            i2c_currentState = I2C_WAITING;
            i2c_nextState = I2C_BMI160_CONF_CMD_CHECK_A;
            i2c_delayCounter = 0;
            IntTrigger(INT_I2C2);
            break;
        }
        case I2C_BMI160_CONF_CMD_CHECK_A: {
            // read the ACC_CONF register.
            i2c_slaveAddress = 0x69;
            i2c_targetRegister = 0x40;
            numberOfBytes = 1;
            currentByte = 0;
            i2c_currentMode = I2C_RECV;

            i2c_currentState = I2C_WAITING;
            i2c_nextState = I2C_BMI160_CONF_CMD_CHECK_B;
            IntTrigger(INT_I2C2);
            break;
        }
        case I2C_BMI160_CONF_CMD_CHECK_B: {
            // check it's what we expected.
            if (recvBuffer[0] == 0b0101010) {
                i2c_currentState = I2C_BMI160_CONF_CMD_CHECK_C;
                i2c_delayCounter = 0;
            } else {
                i2c_currentState = I2C_BMI160_BOOT_CMD_COMPLETE;
            }
            break;
        }
        case I2C_BMI160_CONF_CMD_CHECK_C: {
            // write 4g to ACC_RANGE.
            i2c_slaveAddress = 0x69;
            i2c_targetRegister = 0x41;
            numberOfBytes = 1;
            currentByte = 0;
            i2c_currentMode = I2C_SEND;

            sendBuffer[0] = 0b0101; // configure to 4G
            i2c_currentState = I2C_WAITING;
            i2c_nextState = I2C_BMI160_CONF_CMD_CHECK_D;
            IntTrigger(INT_I2C2);
            break;
        }
        case I2C_BMI160_CONF_CMD_CHECK_D: {
            // write to 0x50 to enable anymotion
            i2c_slaveAddress = 0x69;
            i2c_targetRegister = 0x50;
            numberOfBytes = 1;
            currentByte = 0;
            i2c_currentMode = I2C_SEND;

            sendBuffer[0] = 0b00000111;
            i2c_currentState = I2C_WAITING;
            i2c_nextState = I2C_BMI160_CONF_CMD_CHECK_E;
            IntTrigger(INT_I2C2);
            break;
        }
        case I2C_BMI160_CONF_CMD_CHECK_E: {
            // write to INT_OUT_CTRL
            i2c_slaveAddress = 0x69;
            i2c_targetRegister = 0x53;
            numberOfBytes = 1;
            currentByte = 0;
            i2c_currentMode = I2C_SEND;

            sendBuffer[0] = 0b00001001; // enable INT1, active low, push-pull, edge triggered
            i2c_currentState = I2C_WAITING;
            i2c_nextState = I2C_BMI160_CONF_CMD_CHECK_F;
            IntTrigger(INT_I2C2);
            break;
        }
        case I2C_BMI160_CONF_CMD_CHECK_F: {
            // write to INT_LATCH
            i2c_slaveAddress = 0x69;
            i2c_targetRegister = 0x54;
            numberOfBytes = 1;
            currentByte = 0;
            i2c_currentMode = I2C_SEND;

            sendBuffer[0] = 0b1110; // temp 2.56s
            i2c_currentState = I2C_WAITING;
            i2c_nextState = I2C_BMI160_CONF_CMD_CHECK_G;
            i2c_delayCounter = 0;
            IntTrigger(INT_I2C2);
            break;
        }
        case I2C_BMI160_CONF_CMD_CHECK_G: {
            // write to INT_MAP
            i2c_slaveAddress = 0x69;
            i2c_targetRegister = 0x55;
            numberOfBytes = 1;
            currentByte = 0;
            i2c_currentMode = I2C_SEND;

            sendBuffer[0] = 0b00000100;
            i2c_currentState = I2C_WAITING;
            i2c_nextState = I2C_BMI160_CONF_ANYMOTION;
            i2c_delayCounter = 0;
            IntTrigger(INT_I2C2);
            break;
        }
        case I2C_BMI160_CONF_ANYMOTION: {
            // write to INT_MOTION
            i2c_slaveAddress = 0x69;
            i2c_targetRegister = 0x5F + 1;
            numberOfBytes = 1;
            currentByte = 0;
            i2c_currentMode = I2C_SEND;

            sendBuffer[0] = anymotionThreshold;
            anymotionThresholdChanged = 0;
            i2c_currentState = I2C_WAITING;
            i2c_nextState = I2C_BMI160_CONF_CMD_CHECK_H;
            i2c_delayCounter = 0;
            IntTrigger(INT_I2C2);
            break;
        }
        case I2C_BMI160_CONF_CMD_CHECK_H: {
            // read the configuration registers.
            i2c_slaveAddress = 0x69;
            i2c_targetRegister = 0x50;
            numberOfBytes = 19;
            currentByte = 0;
            i2c_currentMode = I2C_RECV;

            i2c_currentState = I2C_WAITING;
            i2c_nextState = I2C_BMI160_CONF_CMD_CHECK_I;
            IntTrigger(INT_I2C2);
            break;
        }
        case I2C_BMI160_CONF_CMD_CHECK_I: {
            i2c_currentState = I2C_BMI160_CONF_CMD_COMPLETE;
            break;
        }
        case I2C_BMI160_CONF_CMD_COMPLETE: {
            // finally, we can start reading.
            // it takes two timer ticks to do a read
            // so we get to this case every 1ms * 2 = 2ms.
            i2c_delayCounter++;
            i2c_currentMode = I2C_RECV;

            if (i2c_delayCounter >= 100) {
                // read the RESULT register of the OPT3001.
                i2c_delayCounter = 0;

                i2c_slaveAddress = 0x47;
                i2c_targetRegister = 0x00;
                numberOfBytes = 2;
                currentByte = 0;
                i2c_nextState = I2C_OPT3001_READ_COMPLETE;
            } else {
                // read the DATA registers of the BMI160.
                i2c_slaveAddress = 0x69;
                i2c_targetRegister = 0x12;
                numberOfBytes = 6;
                currentByte = 0;
                i2c_nextState = I2C_BMI160_READ;
            }

            i2c_currentState = I2C_WAITING;
            IntTrigger(INT_I2C2);
            break;
        }
        case I2C_BMI160_READ: {
            // convert the things
            SensorBmi160Convert(recvBuffer[1], recvBuffer[0], &gForceX);
            SensorBmi160Convert(recvBuffer[3], recvBuffer[2], &gForceY);
            SensorBmi160Convert(recvBuffer[5], recvBuffer[4], &gForceZ);
            // get the magnitude
            *(currentMagnitudes+currentMagnitudePos) = get_3DPythagoras(gForceX, gForceY, gForceZ);
            currentMagnitudePos++;
            if (currentMagnitudePos == NUMBER_OF_MAGNITUDES) {
                currentMagnitudePos = 0;
                Event_post(boostEvt, speedMask);
            }
            if (anymotionThresholdChanged) {
                i2c_currentState = I2C_BMI160_CONF_ANYMOTION;
            } else {
                i2c_currentState = I2C_BMI160_CONF_CMD_COMPLETE;
            }
            break;
        }
        case I2C_OPT3001_READ_COMPLETE: {
            // breakpoint!
            // conversion stuff
            SensorOpt3001Convert(recvBuffer[0], recvBuffer[1], currentLuxes+currentLuxPos);
            currentLuxPos++;
            if (currentLuxPos == NUMBER_OF_LUXES) {
                currentLuxPos = 0;
                Event_post(boostEvt, lightMask);
            }
            i2c_currentState = I2C_BMI160_CONF_CMD_COMPLETE;
            break;
        }
        case I2C_WAITING: {
            i2c_delayCounter++;
            if (i2c_delayCounter > 5000) {
                // reset, something went wrong!
                i2c_delayCounter = 0;
                i2c_currentState = I2C_VERIFY_OPT3001;
            }
            break;
        }
    }
}

uint8_t get_anymotionThreshold(void) {
    return anymotionThreshold;
}

void set_anymotionThreshold(uint8_t newThresh) {
    anymotionThresholdChanged = 1;
    anymotionThreshold = newThresh;
}

float get_3DPythagoras(float x, float y, float z) {
    return sqrt( pow(x, 2) + pow(y, 2) + pow(z, 2) );
}


void get_gForce(float* x, float* y, float* z) {
    *x = gForceX;
    *y = gForceY;
    *z = gForceZ;
}

float get_currentMagnitude(void) {
    float sum;
    sum = 0;
    int i = 0;
    for (i = 0; i < NUMBER_OF_MAGNITUDES; i++) {
        sum += currentMagnitudes[i];
    }
    return sum / NUMBER_OF_MAGNITUDES;
}

float get_currentLux(void) {
    float sum;
    sum = 0;
    int i = 0;
    for (i = 0; i < NUMBER_OF_LUXES; i++) {
        sum += currentLuxes[i];
    }
    return sum / NUMBER_OF_LUXES;
}

