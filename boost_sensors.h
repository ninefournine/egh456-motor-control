/*
 * boost_sensors.h
 *
 * Module to operate the OPT3001 and BMI160 sensors.
 *
 *  Created on: 28 May 2019
 *      Author: caoj7
 */

#ifndef BOOST_SENSORS_H_
#define BOOST_SENSORS_H_

#include <xdc/runtime/Types.h>
#include <ti/sysbios/knl/Event.h>


extern Types_FreqHz freq;


// Get the g force in three directions -- pass pointers and it will fill them out
void get_gForce(float* x, float* y, float* z);

// Return the current light intensity
float get_currentLux(void);

/*
 * Manage the state machine for the boost sensors. Call this in a loop at about 1kHz
 */
void sensors_process(void);

/*
 * Initialise the boost sensors.
 * Pass in an event handle and two ints.
 * It will fire the first when a light reading is available.
 * It will fire the second when a speed reading is available.
 */
void sensors_init(Event_Handle evt, unsigned int lightMask, unsigned int speedMask);

/*
 * Bind to interrupt 77.
 */
void I2CInterrupt(void);

// Set the anymotion threshold.
void set_anymotionThreshold(uint8_t newThresh);

// get the anymotion threshold.
uint8_t get_anymotionThreshold(void);

// get the current magnitude of all the g forces
float get_currentMagnitude(void);

float get_3DPythagoras(float x, float y, float z);

#endif /* BOOST_SENSORS_H_ */
