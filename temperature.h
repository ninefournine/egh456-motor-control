/*
 * temperature.h
 *
 * Module for operating the TMP107 sensors.
 *
 *  Created on: 21 May 2019
 *      Author: caoj7
 */

#ifndef TEMPERATURE_H_
#define TEMPERATURE_H_

#include <xdc/runtime/Types.h>

extern Types_FreqHz freq;


/*
 * Send things via UART.
 */
void UARTSend(uint32_t base, const uint8_t *pui8Buffer, uint32_t ui32Count);

/*
 * Return the ambient temperature (the first sensor).
 */
float temperature_getAmbient(void);

/*
 * Return the motor temperature (the second sensor.)
 */
float temperature_getMotor(void);

/*
 * Manage the state machine for the temperature sensor.
 * You should call this function in a loop at around 1kHz.
 */
void temperature_process(void);

/*
 * Initialise the temperature sensors.
 * Pass in an event handle and an int. It will fire the event when a temperature reading is available.
 */
void temperature_init(Event_Handle evt, unsigned int eventMask);

/*
 * Interrupt for the temperature sensor. Bind this to interrupt number 76 in a HWI.
 */
void UARTTemperatureInterrupt(void);

float get_temperatureAllowed(void);

void set_temperatureAllowed(float temp);



#endif /* TEMPERATURE_H_ */
