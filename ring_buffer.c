/*
 * ring_buffer.c
 *
 *  Created on: 3 Jun. 2019
 *      Author: caoj7
 */

#include <stdint.h>

#include "ring_buffer.h"

#define NO_OF_BUFFERS 8

float ring_buffers[NO_OF_BUFFERS][BUFFER_LENGTH];
volatile uint16_t read_heads[NO_OF_BUFFERS];
volatile uint16_t write_heads[NO_OF_BUFFERS];

uint8_t updatedFlags[NO_OF_BUFFERS];

void buffers_init() {
    int i;
    for (i = 0; i < NO_OF_BUFFERS; i++) {
        read_heads[i] = 0;
        write_heads[i] = 0;
        int j;
        for (j = 0; j < BUFFER_LENGTH; j++) {
            ring_buffers[i][j] = 0;
        }
    }
}


float get_bufferValue(int bufferID, int i) {
    uint16_t offset = (read_heads[bufferID]+i) % BUFFER_LENGTH;
    return ring_buffers[bufferID][offset];
}

void insert_bufferValue(int bufferID, float value) {
    ring_buffers[bufferID][write_heads[bufferID]] = value;
    if (write_heads[bufferID] == read_heads[bufferID]) {
        read_heads[bufferID]++;
        read_heads[bufferID] = read_heads[bufferID] % BUFFER_LENGTH;
    }
    write_heads[bufferID]++;
    write_heads[bufferID] = write_heads[bufferID] % BUFFER_LENGTH;
    updatedFlags[bufferID] = 1;
}

uint8_t is_updated(int bufferID) {
    if (updatedFlags[bufferID] == 1) {
        updatedFlags[bufferID] = 0;
        return 1;
    }
    return 0;
}

