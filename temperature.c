/*
 * temperature.c
 *
 *  Created on: 21 May 2019
 *      Author: caoj7
 */

#include <stdint.h>
#include <stdbool.h>
#include <ti/drivers/GPIO.h>
#include <_lock.h>

#include "driverlib/uart.h"
#include "driverlib/sysctl.h"
#include "driverlib/interrupt.h"
#include "driverlib/pin_map.h"
#include "driverlib/gpio.h"
#include <ti/sysbios/knl/Event.h>


#include "inc/hw_memmap.h"
#include "inc/hw_ints.h"

#include "Board.h"


#include "temperature.h"

// Buffer for storing UART responses.
char tempUartBuffer[10];
uint8_t tempUartBufferPos;


// The last character received. It's global so that we can see it in the debugger
volatile char characterReceived;

// A timer used for delays.
volatile uint16_t tempUartWaitCount;

uint8_t tempAHigh[3];
uint8_t tempALow[3];
uint8_t tempBHigh[3];
uint8_t tempBLow[3];
uint8_t tempPos;

// State machine for the temperature UART
enum temperatureStateMachine {
    TEMP_INIT_CHECK = 0,
    TEMP_WAITING_FOR_INIT_CHECK,
    TEMP_NOT_INITIALISED,
    TEMP_WAITING_FOR_INIT,
    TEMP_INIT_COMPLETE,
    TEMP_WAITING_FOR_CONFIG_A,
    TEMP_WAITING_FOR_CONFIG_B,
    TEMP_CONFIG_COMPLETE,
    TEMP_WAITING_FOR_CONVERSION_A,
    TEMP_COOLDOWN_A,
    TEMP_WAITING_FOR_CONVERSION_B,
    TEMP_COOLDOWN_B
};


volatile uint8_t tempUartState;

volatile float temperatureAllowed;

Event_Handle evt;
unsigned int eventMask;


// Setup the uart WITHOUT using a hwi
void setup_UART(void) {
    // uart0
    SysCtlPeripheralEnable(SYSCTL_PERIPH_UART0);

    IntMasterEnable();

    GPIOPinConfigure(GPIO_PA0_U0RX);
    GPIOPinConfigure(GPIO_PA1_U0TX);
    GPIOPinTypeUART(GPIO_PORTA_BASE, GPIO_PIN_0 | GPIO_PIN_1);


    UARTConfigSetExpClk(UART0_BASE, freq.lo, 115200,
                        (UART_CONFIG_WLEN_8 | UART_CONFIG_STOP_ONE |
                                                     UART_CONFIG_PAR_NONE));

    // uart7
    SysCtlPeripheralEnable(SYSCTL_PERIPH_UART7);

    IntMasterEnable();

    GPIOPinConfigure(GPIO_PC4_U7RX);
    GPIOPinConfigure(GPIO_PC5_U7TX);
    GPIOPinTypeUART(GPIO_PORTC_BASE, GPIO_PIN_4 | GPIO_PIN_5);


    UARTConfigSetExpClk(UART7_BASE, freq.lo, 9600,
                        (UART_CONFIG_WLEN_8 | UART_CONFIG_STOP_ONE |
                                                     UART_CONFIG_PAR_NONE));
    IntEnable(INT_UART0);
    UARTIntEnable(UART7_BASE, UART_INT_RX | UART_INT_RT);
}


void
UARTSend(uint32_t base, const uint8_t *pui8Buffer, uint32_t ui32Count)
{
    //
    // Loop while there are more characters to send.
    //
    while(ui32Count--)
    {
        //
        // Write the next character to the UART.
        //
        UARTCharPutNonBlocking(base, *pui8Buffer++);
        //UARTCharPut(base, *pui8Buffer++);
    }
}

/*
 * Convert a TMP107 temperature (high and low bits) into a float.
 */
float temperature_convert(uint8_t high, uint8_t low) {
    uint16_t value = (high << 8) | (low & 0b11111100);
    return (float)value / 256;
}

void temperature_init(Event_Handle passedEvent, unsigned int mask) {
    setup_UART();
    tempUartState = TEMP_NOT_INITIALISED; // TEMP_INIT_CHECK doesn't work on all boards?
    temperatureAllowed = 30.0f;
    tempUartBufferPos = 0;
    tempPos = 0;
    eventMask = mask;
    evt = passedEvent;
}

void temperature_process(void) {

    switch (tempUartState) {
        case TEMP_INIT_CHECK: {
            // Send the last address poll.
            uint8_t command[2] = { 0b01010101, 0b01010111 };
            UARTSend(UART7_BASE, command, 2);
            tempUartState = TEMP_WAITING_FOR_INIT_CHECK;
            tempUartWaitCount = 0;
            break;
        }
        case TEMP_WAITING_FOR_INIT_CHECK: {
            // timeout after 100ms.
            // if it didn't respond, assume initialization wasn't done correctly
            tempUartWaitCount++;
            if (tempUartWaitCount >= 100) {
                tempUartState = TEMP_NOT_INITIALISED;
                tempUartWaitCount = 0;
            }
            break;
        }
        case TEMP_NOT_INITIALISED: {
            // Send address initialization.
            uint8_t command[3] = { 0b01010101, 0b10010101, 0b00001101 };
            UARTSend(UART7_BASE, command, 3);
            tempUartState = TEMP_WAITING_FOR_INIT;
            tempUartWaitCount = 0;
            break;
        }
        case TEMP_INIT_COMPLETE: {
            // wait 1250ms after address initialization.
            tempUartWaitCount++;
            if (tempUartWaitCount >= 1250) {
                // Send an individual write.
                uint8_t command[5] = { 0b01010101, 0b00001000, 0b10100001, 0b00000000, 0b01100000 };
                UARTSend(UART7_BASE, command, 5);
                tempUartState = TEMP_WAITING_FOR_CONFIG_A;
                tempUartWaitCount = 0;
            }
            break;
        }
        case TEMP_WAITING_FOR_CONFIG_A: {
            // Timeout after 400ms. There's no way of checking when a write is done but this is probably good enough
            tempUartWaitCount++;
            if (tempUartWaitCount >= 400) {
                // Send an individual write.
                uint8_t command[5] = { 0b01010101, 0b00010000, 0b10100001, 0b00000000, 0b01100000 };
                UARTSend(UART7_BASE, command, 5);
                tempUartState = TEMP_WAITING_FOR_CONFIG_B;
                tempUartWaitCount = 0;
            }
            break;
        }
        case TEMP_WAITING_FOR_CONFIG_B: {
            // Timeout after 400ms. There's no way of checking when a write is done but this is probably good enough
            tempUartWaitCount++;
            if (tempUartWaitCount >= 400) {
                tempUartState = TEMP_CONFIG_COMPLETE;
                tempUartWaitCount = 0;
            }
            break;
        }
        case TEMP_CONFIG_COMPLETE: {
            // Send individual read to address ID 1
            uint8_t command[3] = { 0b01010101, 0b00001010, 0b10100000 };
            UARTSend(UART7_BASE, command, 3);
            tempUartState = TEMP_WAITING_FOR_CONVERSION_A;
            break;
        }
        case TEMP_COOLDOWN_A: {
            // wait 150ms before making another reading.
            tempUartWaitCount++;
            if (tempUartWaitCount >= 150) {
                // send individual read to address ID 2
                uint8_t command[3] = { 0b01010101, 0b00010010, 0b10100000 };
                UARTSend(UART7_BASE, command, 3);
                tempUartState = TEMP_WAITING_FOR_CONVERSION_B;
                tempUartWaitCount = 0;
            }
            break;
        }
        case TEMP_COOLDOWN_B: {
            // wait 150ms before making another reading.
            tempUartWaitCount++;
            if (tempUartWaitCount >= 150) {
                // go back to the other state.
                tempUartState = TEMP_CONFIG_COMPLETE;
                tempUartWaitCount = 0;
            }
            break;
        }
        default:
            break;
    }
}


void UARTTemperatureInterrupt(void) {
    uint32_t ui32Status;

    /* Clear interrupts */
    ui32Status = UARTIntStatus(UART7_BASE, true);
    UARTIntClear(UART7_BASE, ui32Status);

    if (UARTCharsAvail(UART7_BASE))
    {
        characterReceived = UARTCharGet(UART7_BASE);
        switch (tempUartState) {
            case TEMP_WAITING_FOR_INIT_CHECK: {
                // expecting 3 interrupts (2 from echo, 1 response)
                tempUartBuffer[tempUartBufferPos] = characterReceived;
                tempUartBufferPos++;
                if (tempUartBufferPos >= 3) {
                    uint8_t val = tempUartBuffer[2];
                    if (val == 0b00010011) {
                        tempUartState = TEMP_INIT_COMPLETE;
                    } else {
                        tempUartState = TEMP_NOT_INITIALISED;
                    }
                    tempUartBufferPos = 0;
                }

                break;
            }
            case TEMP_WAITING_FOR_INIT: {
                // wait for 5 interrupts (3 from echo, two response bytes)
                tempUartBufferPos++;
                if (tempUartBufferPos >= 5) {
                    tempUartState = TEMP_INIT_COMPLETE;
                    tempUartBufferPos = 0;
                }
                break;
            }
            case TEMP_WAITING_FOR_CONVERSION_A: {
                // wait for 5 interrupts (3 from echo, 2 response bytes)
                tempUartBuffer[tempUartBufferPos] = characterReceived;
                tempUartBufferPos++;
                if (tempUartBufferPos >= 5) {
                    tempUartState = TEMP_COOLDOWN_A;
                    tempUartBufferPos = 0;
                    tempPos = tempPos % 3;
                    tempAHigh[tempPos] = tempUartBuffer[4];
                    tempALow[tempPos] = tempUartBuffer[3];
                    tempPos++;
                }
                break;
            }
            case TEMP_WAITING_FOR_CONVERSION_B: {
                // wait for 5 interrupts (3 from echo, 2 response bytes)
                tempUartBuffer[tempUartBufferPos] = characterReceived;
                tempUartBufferPos++;
                if (tempUartBufferPos >= 5) {
                    tempUartState = TEMP_COOLDOWN_B;
                    tempUartBufferPos = 0;
                    tempPos = tempPos % 3;
                    tempBHigh[tempPos] = tempUartBuffer[4];
                    tempBLow[tempPos] = tempUartBuffer[3];
                    tempPos++;

                    if (tempPos >= 3) {
                        Event_post(evt, eventMask);
                    }

                }
                break;
            }
            default:
                break;
        }
    }
}

float temperature_getAmbient(void) {
    //return temperature_convert( tempAHigh[tempPos - 1], tempALow[tempPos - 1] );

    // get average
    uint16_t av_high = (tempAHigh[0] + tempAHigh[1] + tempAHigh[2]) / 3;
    uint16_t av_low = (tempALow[0] + tempALow[1] + tempALow[2]) / 3;
    return temperature_convert( (uint8_t)av_high, (uint8_t)av_low  );

}

float temperature_getMotor(void) {
    //return temperature_convert( tempBHigh[tempPos - 1], tempBLow[tempPos - 1]  );

    // get average
    uint16_t av_high = (tempBHigh[0] + tempBHigh[1] + tempBHigh[2]) / 3;
    uint16_t av_low = (tempBLow[0] + tempBLow[1] + tempBLow[2]) / 3;
    return temperature_convert( (uint8_t)av_high, (uint8_t)av_low  );
}

float get_temperatureAllowed(void) {
    return temperatureAllowed;
}

void set_temperatureAllowed(float temp) {
    temperatureAllowed = temp;
}
