/*
 * motor.h
 *
 *  Created on: 2 Jun. 2019
 *      Author: caoj7
 */

#ifndef MOTOR_H_
#define MOTOR_H_

void motor_init(void);
void AverageRPM(void);
void motor_process(void);

void setTargetRPM(uint16_t newRPM);
uint16_t getTargetRPM(void);
uint16_t getCurrentRPM(void);

uint16_t getCurrentDuty(void);

void setEnable(uint8_t enabled);

void setStoppingState(uint8_t enabled);

void HallEffectCallback(unsigned int index);

void setEstop(uint8_t enabled);

uint8_t getEstop(void);

void setReverse(uint8_t reversed);

uint8_t getEnable(void);



#endif /* MOTOR_H_ */
