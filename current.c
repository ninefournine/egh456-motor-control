/*
 * current.c
 *
 *  Created on: 5 Jun. 2019
 *      Author: caoj7
 */

#include <stdint.h>
#include <stdbool.h>

#include "inc/hw_memmap.h"
#include "inc/hw_ints.h"

#include "driverlib/adc.h"
#include "driverlib/sysctl.h"
#include "driverlib/gpio.h"
#include "driverlib/interrupt.h"

#include "ring_buffer.h"
#include "wave_table.h"
#include "motor.h"


enum ADCStateMachine {
    ADC_READY_TO_SAMPLE = 0,
    ADC_WAITING_FOR_SAMPLE
};

enum ADCStateMachine ADC_currentState;

// Sample buffer for the ADC. It must be the depth of the FIFO of the sample sequence
// here, it's 4.
uint32_t ADCRecvBuffer[4];

uint32_t ADCSamplesA[30];
uint32_t ADCSamplesB[30];
uint8_t ADCSamplePointer;

uint32_t threshold;

uint8_t fakeWaveIndex;

void ADCInterrupt(void) {
    // Clear the ADC sample sequence interrupt.
    ADCIntClear(ADC1_BASE, 2);

    // Get the results.
    ADCSequenceDataGet(ADC1_BASE, 2, ADCRecvBuffer);

    ADC_currentState = ADC_READY_TO_SAMPLE;

}

void setCurrentThreshold(uint32_t newThreshold) {
    threshold = newThreshold;
}

uint32_t getCurrentThreshold(void) {
    return threshold;
}

#define FAKE_CURRENT_MULTIPLAR 15

void adc_process(void) {
    ADCSamplePointer++;
    if (ADCSamplePointer > 80) {
        ADCSamplePointer = 0;
        insert_bufferValue(5, (float)ADCRecvBuffer[0]);
        insert_bufferValue(6, (float)ADCRecvBuffer[1]);
        fakeWaveIndex += 5;
        insert_bufferValue(7, (float)sine_wave[fakeWaveIndex]*FAKE_CURRENT_MULTIPLAR);
        if (ADCRecvBuffer[0] > threshold || ADCRecvBuffer[1] > threshold || sine_wave[fakeWaveIndex]*FAKE_CURRENT_MULTIPLAR > threshold) {
            setEstop(1);
        }
    }
    switch (ADC_currentState) {
        case ADC_READY_TO_SAMPLE: {
            // fire a processor signal to start a conversion on sample sequence 2.
            ADCProcessorTrigger(ADC1_BASE, 2);
            ADC_currentState = ADC_WAITING_FOR_SAMPLE;


//            //
//            // Wait for conversion to be completed.
//            //
//            while(!ADCIntStatus(ADC0_BASE, 2, false))
//            {
//            }
//            GPIO_toggle(Board_LED0);
            break;
        }
    }
}

void adc_init(void) {

    ADCSamplePointer = 0;
    fakeWaveIndex = 0;
    threshold = 5000;

    SysCtlPeripheralEnable(SYSCTL_PERIPH_ADC1);

    GPIOPinTypeADC(GPIO_PORTE_BASE, GPIO_PIN_3);
    GPIOPinTypeADC(GPIO_PORTD_BASE, GPIO_PIN_7);


    //
    // Wait for the ADC1 module to be ready.
    //
    while(!SysCtlPeripheralReady(SYSCTL_PERIPH_ADC1))
    {
    }

    // use the internal 16 Mhz clock, no prescalar
    // ADCClockConfigSet(ADC0_BASE, ADC_CLOCK_SRC_PIOSC | ADC_CLOCK_RATE_FULL, 1);
    // set 32x sampling
    // which means we're averaging every 32 samples IN HARDWARE woohoo
    ADCHardwareOversampleConfigure(ADC1_BASE, 64);

    // set reference to the internal 3V source
    ADCReferenceSet(ADC1_BASE, ADC_REF_INT);

    // disable the sequence so that we can configure it.
    ADCSequenceDisable(ADC1_BASE, 2);
    // Enable sequence 2 -- sequence 3 is being used by the touchscreen!
    ADCSequenceConfigure(ADC1_BASE, 2, ADC_TRIGGER_PROCESSOR, 0);

    // Step 0 of Sequence 2 - read AIN0
    //ADCSequenceStepConfigure(ADC0_BASE, 2, 0, ADC_CTL_CH0);
    // Step 1 of Sequence 2 - read AIN4
    ADCSequenceStepConfigure(ADC1_BASE, 2, 0, ADC_CTL_CH0);
    ADCSequenceStepConfigure(ADC1_BASE, 2, 1, ADC_CTL_CH4| ADC_CTL_IE | ADC_CTL_END);

    // Enable the interrupt for the ADC
    ADCIntEnable(ADC1_BASE, 2);
    IntEnable(INT_ADC1SS2);


    // enable sample sequence 2.
    ADCSequenceEnable(ADC1_BASE, 2);

    //
    // Clear the interrupt status flag.  This is done to make sure the
    // interrupt flag is cleared before we sample.
    //
    ADCIntClear(ADC1_BASE, 2);

    ADC_currentState = ADC_READY_TO_SAMPLE;
}
