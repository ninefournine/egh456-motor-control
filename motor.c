/*
 * motor.c
 *
 *  Created on: 2 Jun. 2019
 *      Author: caoj7
 */

#include <stdint.h>
#include <stdbool.h>
#include <ti/drivers/PWM.h>
#include <ti/sysbios/knl/Clock.h>
#include <ti/sysbios/knl/Swi.h>
#include <ti/drivers/GPIO.h>

#include "inc/hw_ints.h"
#include "driverlib/interrupt.h"

#include "motor.h"
#include "Board.h"

/****
 * MOTOR CONTROL STUFF STARTS HERE
 */
#define MAXPWMVALUE 65535
#define SECONDSPERMINUTE 60

#define MOTOR_K_RATIO 10

// approx.
#define maxAccelerationRPMPerClockDivision 1
#define dutyCyclesPerRPM 10


#define MAX_NUMBER_OF_SAMPLES 24


/* Hall Effect things */
volatile uint8_t hallEffectBitfield; // bit0 = halla, bit1 = hallb, bit2 = hallc
volatile uint32_t prevClockTicks;
volatile uint32_t clockTickBuffer[MAX_NUMBER_OF_SAMPLES];
volatile uint8_t clockTickBufferIndex;

volatile uint8_t motorEnabled;
volatile uint8_t currentlyStopping;
volatile uint8_t estop;

volatile uint8_t numberOfSamples;


/* PWM objects */
PWM_Params pwmParams;
PWM_Handle pwmHandleHA;

/* SWI for averaging the hall effect */
Swi_Handle hallSwi;
Swi_Params hallSwiParams;

volatile float dutyCyclePercentage;
volatile uint16_t newDutyCycle;
volatile uint16_t currentDutyCycle;

volatile int currentRPM;
volatile int targetRPM;
volatile uint16_t motorStoppedCounter;

uint8_t motorStartCounter;


uint16_t getTargetRPM(void) {
    return targetRPM;
}

void setTargetRPM(uint16_t new) {
    if (currentlyStopping || estop) {
        return;
    }
    targetRPM = new;
}

uint16_t getCurrentRPM(void) {
    return currentRPM;
}

uint16_t getCurrentDuty(void) {
    return currentDutyCycle;
}


void AverageRPM() {
    uint64_t value;
    value = 0;
    int i = 0;
    for (i = 0; i < numberOfSamples; i++) {
        value += clockTickBuffer[i];
    }
    value = value / numberOfSamples;
    currentRPM = (10000 * 60) / (float)((value * 24));
    //reportFlag = 1;
    numberOfSamples = currentRPM / 110 + 2;

}

void SendPWMToMotor(uint16_t dutyCycle)
{
    // safety first!
    if (dutyCycle > 30000) {
        dutyCycle = 30000;
    }
    if (dutyCycle < 3000) {
        dutyCycle = 3000;
        if (currentlyStopping) {
            currentlyStopping = 0;
            setEnable(0);
            estop = 0;
        }
    }
    currentDutyCycle = dutyCycle;
    PWM_setDuty(pwmHandleHA, dutyCycle);
}

void motor_process(void) {
    motorStoppedCounter++;
    // 100 ticks = 100ms
    // if motorstoppedcounter reaches 100 we're stopped for sure
    if (motorStoppedCounter >= 100) {
        currentRPM = 0;
        motorStoppedCounter = 0;
        //reportFlag = 1;
        GPIO_write(Board_LED0, 0);
    }

    if (currentRPM != targetRPM && motorEnabled) {
        /*
        newDutyCycle = CalculateNewDutyCycle();
        SendPWMToMotor(newDutyCycle);
        */

        int diff = abs(currentDutyCycle - targetRPM*10.41);

        if (diff > 40) {
            diff = 40;
        }

        if (diff == 0)
        {
            diff = 1;
        }

        if (estop) {
            diff = 80;
        }

        if (currentRPM < targetRPM) {
            SendPWMToMotor(currentDutyCycle + diff);
        } else {
            SendPWMToMotor(currentDutyCycle - diff);
        }

    }
}



// callback function for the hall effect sensors.
void HallEffectCallback(unsigned int index) {
    uint32_t currentClockTicks = Clock_getTicks();
    uint32_t difference = currentClockTicks - prevClockTicks;
    prevClockTicks = currentClockTicks;
    clockTickBuffer[clockTickBufferIndex] = difference;
    clockTickBufferIndex++;
    if (clockTickBufferIndex >= numberOfSamples) {
        Swi_post(hallSwi);
        clockTickBufferIndex = 0;
    }

    GPIO_toggle(Board_LED0);
    GPIO_clearInt(MOTOR_HALLA);
    GPIO_clearInt(MOTOR_HALLB);
    GPIO_clearInt(MOTOR_HALLC);
    int valuea = GPIO_read(MOTOR_HALLA);
    int valueb = GPIO_read(MOTOR_HALLB);
    int valuec = GPIO_read(MOTOR_HALLC);
    GPIO_write(MOTOR_INLA, valuea);
    GPIO_write(MOTOR_INHB, valueb);
    GPIO_write(MOTOR_INLB, valuec);

    motorStoppedCounter = 0;

}

void setStoppingState(uint8_t enabled) {
    currentlyStopping = enabled;
    if (enabled) {
        targetRPM = 0;
    }
    if (currentRPM == 0) {
        setEnable(0);
    }
}

void setEstop(uint8_t enabled) {
    estop = enabled;
    setStoppingState(1);
}

uint8_t getEstop(void) {
    return estop;
}

void setEnable(uint8_t enabled) {
    GPIO_write(MOTOR_ENABLE, enabled);
    GPIO_write(Board_LED1, enabled);

    // pull the other pins low initially
//    GPIO_write(MOTOR_INLA, 1);
//    GPIO_write(MOTOR_INHB, 1);
//    GPIO_write(MOTOR_INLB, 1);
    motorStartCounter = 0;

    if (enabled) {
        estop = 0;
    }
    currentlyStopping = 0;
    motorEnabled = enabled;
    HallEffectCallback(1);
}

uint8_t getEnable(void) {
    return motorEnabled;
}

void setReverse(uint8_t reversed) {
    GPIO_write(MOTOR_INHC, reversed);
}

void motor_init(void) {
    targetRPM = 300;
    /* Make the SWI */
    Swi_Params_init(&hallSwiParams);
    hallSwi = Swi_create((Swi_FuncPtr)AverageRPM, &hallSwiParams, NULL);

    currentlyStopping = 0;
    estop = 0;
    numberOfSamples = 24;

    /* Setup GPIO interrupts for the three Hall Effect sensors. */
    GPIO_setCallback(MOTOR_HALLA, HallEffectCallback);
    GPIO_setCallback(MOTOR_HALLB, HallEffectCallback);
    GPIO_setCallback(MOTOR_HALLC, HallEffectCallback);
    GPIO_enableInt(MOTOR_HALLA);
    GPIO_enableInt(MOTOR_HALLB);
    GPIO_enableInt(MOTOR_HALLC);

    // pull INHC low, INLC high
    GPIO_write(MOTOR_INHC, 0);
    GPIO_write(MOTOR_INLC, 1);


    // drive MOTOR_MODE high-impedence??? idk how this works
    // GPIO_write(MOTOR_MODE, 1);


    // pull MOTOR_ENABLE low
    setEnable(0);
    motorEnabled = 0;


    /* Make some PWM instances. */
    pwmParams.period = 50;
    pwmParams.dutyMode = PWM_DUTY_SCALAR;
    pwmParams.polarity = PWM_POL_ACTIVE_HIGH;

    pwmHandleHA = PWM_open(Board_PWM0, &pwmParams);


    SendPWMToMotor(0);
}

