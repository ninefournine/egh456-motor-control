/*
 * calendar.h
 *
 *  Created on: 4 Jun. 2019
 *      Author: caoj7
 */

#ifndef CALENDAR_H_
#define CALENDAR_H_

#include <xdc/runtime/Types.h>

extern Types_FreqHz freq;

void getPrettyTimeAndDate(char* p);
void calendar_init(void);

bool DateTimeDisplayGet(char *pcBuf, uint32_t ui32BufSize);

#endif /* CALENDAR_H_ */
