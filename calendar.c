/*
 * calendar.c
 *
 *  Created on: 4 Jun. 2019
 *      Author: caoj7
 */

#include <stdint.h>
#include <time.h>
#include <stdbool.h>

#include "utils/ustdlib.h"
#include "driverlib/hibernate.h"
#include "driverlib/sysctl.h"


#include "calendar.h"

/*
 * HIBERNATE STUFF STARTS HERE
 */

static char *g_ppcMonth[12] =
{
    "Jan",
    "Feb",
    "Mar",
    "Apr",
    "May",
    "Jun",
    "Jul",
    "Aug",
    "Sep",
    "Oct",
    "Nov",
    "Dec"
};


/* variables for the time! */
uint32_t g_ui32MonthIdx, g_ui32DayIdx, g_ui32YearIdx;
uint32_t g_ui32HourIdx, g_ui32MinIdx;

/* sneaky sneaky set it to our demonstration time */
void
DateTimeDefaultSet(void)
{
    g_ui32MonthIdx = 5;
    g_ui32DayIdx = 5;
    g_ui32YearIdx = 19;
    g_ui32HourIdx = 13;
    g_ui32MinIdx = 50;
}

void
DateTimeSet(void)
{
    struct tm sTime;

    //
    // Get the latest date and time.  This is done here so that unchanged
    // parts of date and time can be written back as is.
    //
    HibernateCalendarGet(&sTime);

    //
    // Set the date and time values that are to be updated.
    //
    sTime.tm_hour = g_ui32HourIdx;
    sTime.tm_min = g_ui32MinIdx;
    sTime.tm_mon = g_ui32MonthIdx;
    sTime.tm_mday = g_ui32DayIdx;
    sTime.tm_year = 100 + g_ui32YearIdx;

    //
    // Update the calendar logic of hibernation module with the requested data.
    //
    HibernateCalendarSet(&sTime);
}

bool
DateTimeGet(struct tm *sTime)
{
    //
    // Get the latest time.
    //
    HibernateCalendarGet(sTime);

    //
    // Is valid data read?
    //
    if(((sTime->tm_sec < 0) || (sTime->tm_sec > 59)) ||
       ((sTime->tm_min < 0) || (sTime->tm_min > 59)) ||
       ((sTime->tm_hour < 0) || (sTime->tm_hour > 23)) ||
       ((sTime->tm_mday < 1) || (sTime->tm_mday > 31)) ||
       ((sTime->tm_mon < 0) || (sTime->tm_mon > 11)) ||
       ((sTime->tm_year < 100) || (sTime->tm_year > 199)))
    {
        //
        // No - Let the application know the same by returning relevant
        // message.
        //
        return false;
    }

    //
    // Return that new data is available so that it can be displayed.
    //
    return true;
}


bool
DateTimeDisplayGet(char *pcBuf, uint32_t ui32BufSize)
{
    static uint32_t ui32SecondsPrev = 0xFF;
    struct tm sTime;
    uint32_t ui32Len;

    //
    // Get the latest date and time and check the validity.
    //
    if(DateTimeGet(&sTime) == false)
    {
        //
        // Invalid - Force set the date and time to default values and return
        // false to indicate no information to display.
        //
        return false;
    }

    //
    // If date and time is valid, check if seconds have updated from previous
    // visit.
    //
    if(ui32SecondsPrev == sTime.tm_sec)
    {
        //
        // No - Return false to indicate no information to display.
        //
        return false;
    }

    //
    // If valid new date and time is available, update a local variable to keep
    // track of seconds to determine new data for next visit.
    //
    ui32SecondsPrev = sTime.tm_sec;

    //
    // Format the date and time into a user readable format.
    //
    ui32Len = usnprintf(pcBuf, ui32BufSize, "%s %02u, 20%02u  ",
                        g_ppcMonth[sTime.tm_mon], sTime.tm_mday,
                        sTime.tm_year - 100);
    usnprintf(&pcBuf[ui32Len], ui32BufSize - ui32Len, "%02u : %02u : %02u",
              sTime.tm_hour, sTime.tm_min, sTime.tm_sec);

    //
    // Return true to indicate new information to display.
    //
    return true;
}


void calendar_init(void) {
    SysCtlPeripheralEnable(SYSCTL_PERIPH_HIBERNATE);
    HibernateEnableExpClk(freq.lo);
    HibernateClockConfig(HIBERNATE_OSC_LOWDRIVE);
    HibernateRTCEnable();
    HibernateCounterMode(HIBERNATE_COUNTER_24HR);
    DateTimeDefaultSet();
    DateTimeSet();


}
