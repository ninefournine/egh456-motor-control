/*
 * gui.h
 *
 *  Created on: 2 Jun. 2019
 *      Author: caoj7
 */

#ifndef GUI_H_
#define GUI_H_

#include <xdc/std.h>
#include <xdc/runtime/Types.h>

extern Types_FreqHz freq;

void StartStopBttnPress(tWidget *psWidget);

void guiTaskFxn(UArg arg0, UArg arg1);

void gui_init(void);

void set_theDay(uint8_t the_day);


#endif /* GUI_H_ */
